//
//  DetailViewController.m
//  Contacts
//
//  Created by Mark Farrell on 3/16/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()

@end

@implementation DetailViewController

#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem {
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
            
        // Update the view.
        [self configureView];
    }
}

- (void)configureView {
    // Update the user interface for the detail item.
    if (self.detailItem) {
        self.detailDescriptionLabel.text = [NSString stringWithFormat:@""];
        //self.detailDescriptionLabel.text = [[self.detailItem valueForKey:@"name"] description];
        
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self configureView];
    UILabel*nameLbl = [[UILabel alloc]initWithFrame:CGRectMake(25, 100, 75, 50)];
    nameLbl.text = [NSString stringWithFormat:@"Name: "];
    [self.view addSubview:nameLbl];
    
    name = [[UITextField alloc]initWithFrame:CGRectMake(100, 100, 200, 50)];
    name.text = [[self.detailItem valueForKey:@"name"] description];
    [self.view addSubview:name];
    
    UITapGestureRecognizer* tgrName = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(nameTapped:)];
    [name setUserInteractionEnabled:YES];
    [name addGestureRecognizer:tgrName];
    
    UILabel* phoneLbl = [[UILabel alloc]initWithFrame:CGRectMake(nameLbl.frame.origin.x, nameLbl.frame.origin.y + 75, nameLbl.frame.size.width, nameLbl.frame.size.height)];
    phoneLbl.text = [NSString stringWithFormat:@"Phone: "];
    [self.view addSubview:phoneLbl];
    
    UILabel* phone = [[UILabel alloc]initWithFrame:CGRectMake(name.frame.origin.x, name.frame.origin.y + 75, name.frame.size.width, name.frame.size.height)];
    phone.text = [[self.detailItem valueForKey:@"phone"]description];
    [phone setUserInteractionEnabled:YES];
    [self.view addSubview:phone];
    
    UITapGestureRecognizer* tgrPhone = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(phoneTapped:)];
    [phone addGestureRecognizer:tgrPhone];
    
    UILabel* emailLbl = [[UILabel alloc]initWithFrame:CGRectMake(phoneLbl.frame.origin.x, phoneLbl.frame.origin.y + 75, phoneLbl.frame.size.width, phoneLbl.frame.size.height)];
    emailLbl.text = [NSString stringWithFormat:@"Email: "];
    [self.view addSubview:emailLbl];
    
    UILabel* email = [[UILabel alloc]initWithFrame:CGRectMake(phone.frame.origin.x, phone.frame.origin.y + 75, phone.frame.size.width, phone.frame.size.height)];
    email.text = [[self.detailItem valueForKey:@"email"]description];
    [self.view addSubview:email];
    
    UILabel* addressLbl = [[UILabel alloc]initWithFrame:CGRectMake(emailLbl.frame.origin.x, emailLbl.frame.origin.y + 75, emailLbl.frame.size.width, emailLbl.frame.size.height)];
    addressLbl.text = [NSString stringWithFormat:@"Address: "];
    [self.view addSubview:addressLbl];
    
    UILabel* address = [[UILabel alloc]initWithFrame:CGRectMake(email.frame.origin.x, email.frame.origin.y + 75, email.frame.size.width, email.frame.size.height)];
    address.text = [[self.detailItem valueForKey:@"address"]description];
    [address setUserInteractionEnabled:YES];
    [self.view addSubview:address];
    
    UITapGestureRecognizer* tgrAddress = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(addressTapped:)];
    [address addGestureRecognizer:tgrAddress];
    
    
   
    
    
}

-(void)nameTapped:(UILabel*)sender{
    
    name.backgroundColor = [UIColor greenColor];
 
}

-(void)phoneTapped:(id)sender{
    NSString *phNo = [_detailItem valueForKey:@"phone"];
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:+%@",phNo]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    } else {
        NSLog(@"Call Failed");
    }
}

-(void)addressTapped:(id)sender{
    MapViewController* mvc = [[MapViewController new]init];
    mvc.detailItem = self.detailItem;
    [self.navigationController pushViewController:mvc animated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
