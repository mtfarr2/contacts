//
//  AddNewContact.h
//  Contacts
//
//  Created by Mark Farrell on 3/19/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
@interface AddNewContact : UIViewController{
    UITextField* name;
    UITextField* phone;
    UITextField* email;
    UITextField* address;
    
}
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end
