//
//  ViewEditViewController.m
//  Contacts
//
//  Created by Mark Farrell on 3/24/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import "ViewEditViewController.h"

@interface ViewEditViewController ()

@end

@implementation ViewEditViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor blackColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
