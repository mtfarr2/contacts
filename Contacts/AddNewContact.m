//
//  AddNewContact.m
//  Contacts
//
//  Created by Mark Farrell on 3/19/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import "AddNewContact.h"

@interface AddNewContact ()

@end

@implementation AddNewContact

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    
    UILabel* header = [[UILabel alloc]initWithFrame:CGRectMake(50, 100, self.view.frame.size.width - 100, 50)];
    header.text = [NSString stringWithFormat:@"Enter Contact Info"];
    header.font = [UIFont fontWithName:@"Marker Felt" size:30];
    [self.view addSubview:header];
    
    name = [[UITextField alloc]initWithFrame:CGRectMake(50, header.frame.origin.y + 75, self.view.frame.size.width - 100, 50)];
    name.placeholder = [NSString stringWithFormat:@" Name"];
    [name.layer setBorderColor:[[UIColor blackColor]CGColor]];
    name.borderStyle = UITextBorderStyleBezel;
    [self.view addSubview:name];
    
    phone = [[UITextField alloc]initWithFrame:CGRectMake(50, name.frame.origin.y + 75, self.view.frame.size.width - 100, 50)];
    phone.placeholder = [NSString stringWithFormat:@" Phone Number"];
    phone.borderStyle = UITextBorderStyleBezel;
    [self.view addSubview:phone];
    
    email = [[UITextField alloc]initWithFrame:CGRectMake(50, phone.frame.origin.y + 75, self.view.frame.size.width - 100, 50)];
    email.placeholder = [NSString stringWithFormat:@" Email Address"];
    email.borderStyle = UITextBorderStyleBezel;
    [self.view addSubview:email];
    
    address = [[UITextField alloc]initWithFrame:CGRectMake(50, email.frame.origin.y + 75, self.view.frame.size.width - 100, 50)];
    address.placeholder = [NSString stringWithFormat:@" Address"];
    address.borderStyle = UITextBorderStyleBezel;
    [self.view addSubview:address];
    
    UIButton* submit = [UIButton buttonWithType:UIButtonTypeCustom];
    submit.frame = CGRectMake(50, address.frame.origin.y + 100, self.view.frame.size.width - 100, 50);
    [submit setBackgroundImage:[UIImage imageNamed:@"SubmitBtnDepressed"] forState:UIControlStateNormal];
    [submit setBackgroundImage:[UIImage imageNamed:@"SubmitBtnPressed"] forState:UIControlStateHighlighted];
    [submit addTarget:self action:@selector(submitButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:submit];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

-(void)submitButtonTouched:(id)sender{
    NSManagedObjectContext *context = self.managedObjectContext;
    NSManagedObject *contactInfo = [NSEntityDescription
                            insertNewObjectForEntityForName:@"Contact"
                            inManagedObjectContext:context];
    [contactInfo setValue:name.text forKey:@"name"];
    [contactInfo setValue:phone.text forKey:@"phone"];
    [contactInfo setValue:email.text forKey:@"email"];
    [contactInfo setValue:address.text forKey:@"address"];
    NSError *error;
    if (![context save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
