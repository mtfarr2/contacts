//
//  MapViewController.h
//  Contacts
//
//  Created by Mark Farrell on 3/25/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface MapViewController : UIViewController <MKMapViewDelegate, CLLocationManagerDelegate>{
    MKMapView* map;
    CLLocationManager* locationManager;
}

@property (strong, nonatomic) id detailItem;
@end
